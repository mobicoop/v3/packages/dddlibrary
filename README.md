# Mobicoop V3 - Domain Driven Design Library package

Library of Domain Driven Design utilities / base building blocks for Mobicoop Platform V3.

Heavily inspired by https://github.com/Sairyss/domain-driven-hexagon (wonderful job !).

## Installation

```bash
npm install --save @mobicoop/ddd-library
```

## License

Mobicoop V3 - Domain Driven Design Library package is [AGPL licensed](LICENSE).
