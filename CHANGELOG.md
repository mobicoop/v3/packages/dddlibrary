# Changelog

All notable changes to this project will be documented in this file. See [commit-and-tag-version](https://github.com/absolute-version/commit-and-tag-version) for commit guidelines.

## [2.6.0](https://gitlab.mobicoop.io/v3/packages/dddlibrary/compare/2.5.4...2.6.0) (2025-02-19)

### Features

- **mapper:** Deprecate Mapper and split into 2 dedicated mappers ([9f27865](https://gitlab.mobicoop.io/v3/packages/dddlibrary/commit/9f27865c448a1c868f3a8a68994068636e1e48fd))

## [2.5.4](https://gitlab.mobicoop.io/v3/packages/dddlibrary/compare/2.5.3...2.5.4) (2025-01-17)

### Bug Fixes

- **pagination:** use zero-based pagination in calculations, as documented and defaulted in queries ([675bfcc](https://gitlab.mobicoop.io/v3/packages/dddlibrary/commit/675bfccc5e2451a71e970cf1edd40cc6e8061c0f))

## [2.5.3](https://gitlab.mobicoop.io/v3/packages/dddlibrary/compare/v2.5.2...v2.5.3) (2025-01-15)

## [2.5.2](https://gitlab.mobicoop.io/v3/packages/dddlibrary/compare/v2.4.0...v2.5.2) (2025-01-15)

### Bug Fixes

- **dto:** require at least one item per page ([48868c1](https://gitlab.mobicoop.io/v3/packages/dddlibrary/commit/48868c1372f52770aaa1b87f7d8bed1a74b5a281))

## [2.5.1](https://gitlab.mobicoop.io/v3/packages/dddlibrary/compare/4cd10e33...v2.5.1) (2024-05-30)

- Expose the error handling of write operations in the base repository to the child classes
