/**
 * Zero-based page calculator
 */
export class Paginator {
  static pageNumber = (
    numberOfItems: number,
    perPage: number,
    pageRequired: number,
  ): number => {
    if (numberOfItems <= perPage) return 0;
    const numberOfPages: number = Math.ceil(numberOfItems / perPage);
    if (numberOfPages <= pageRequired) return numberOfPages - 1;
    return pageRequired;
  };

  static pageItems = <T>(
    items: Array<T>,
    page: number,
    perPage: number,
  ): Array<T> => items.slice(page * perPage, (page + 1) * perPage);
}
