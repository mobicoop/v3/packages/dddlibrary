import { RpcExceptionCode } from '../../exceptions/rpc-exception.codes.enum';
import { Injectable, ValidationPipe } from '@nestjs/common';
import { RpcException } from '@nestjs/microservices';

@Injectable()
export class RpcValidationPipe extends ValidationPipe {
  createExceptionFactory() {
    return (validationErrors = []) => {
      return new RpcException({
        code: RpcExceptionCode.INVALID_ARGUMENT,
        message: this.flattenValidationErrors(validationErrors),
      });
    };
  }
}
