/** Consider creating a bunch of shared custom utility
 * types for different situations.
 * Alternatively you can use a library like
 * https://github.com/andnp/SimplyTyped
 */
export * from './object-literal.type';
