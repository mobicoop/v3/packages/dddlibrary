export interface LoggerPort {
  log(message: any, context: string): void;
  error(message: any, stack: string, context: string): void;
  warn(message: any, stack: string, context: string): void;
  debug(message: any): void;
}
