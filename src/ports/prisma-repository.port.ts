export interface PrismaRepositoryPort<Entity> {
  findUnique(options: any): Promise<Entity>;
  findFirst(options: any): Promise<Entity>;
  findMany(options: any): Promise<Entity[]>;
  create(entity: any): Promise<Entity>;
  update(entity: any): Promise<Entity>;
  updateWhere(where: any, entity: any): Promise<Entity>;
  delete(entity: any): Promise<Entity>;
  count(where: any): Promise<number>;
}

export interface PrismaRawRepositoryPort {
  $queryRaw<T = unknown>(
    query: TemplateStringsArray,
    ...values: any[]
  ): Promise<T>;
  $queryRawUnsafe<T = unknown>(query: string, ...values: any[]): Promise<T>;
  $transaction(statements: any[]): any;
  $executeRawUnsafe(command: any): any;
}
