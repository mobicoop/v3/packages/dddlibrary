export interface MessagePublisherPort {
  publish(routingKey: string, message: string): void;
}
