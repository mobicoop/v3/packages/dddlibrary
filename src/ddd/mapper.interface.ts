import { Entity } from './entity.base';

export interface PersistenceMapper<
  DomainEntity extends Entity<any>,
  DBReadModel,
  DBWriteModel,
> {
  toPersistence(entity: DomainEntity, update?: boolean): DBWriteModel;
  toDomain(record: DBReadModel): DomainEntity;
}

export interface InterfaceMapper<DomainEntity extends Entity<any>, Response> {
  toResponse(entity: DomainEntity): Response;
}

/**
 * @deprecated use PersistenceMapper and/or InterfaceMapper
 */
export interface Mapper<
  DomainEntity extends Entity<any>,
  DBReadModel,
  DBWriteModel,
  Response = any,
> {
  toPersistence?(entity: DomainEntity, update?: boolean): DBWriteModel;
  toDomain?(record: DBReadModel): DomainEntity;
  toResponse?(entity: DomainEntity): Response;
}

/**
 * Mapper to be used when model contains unsupported DB types (eg. geography, geometry...)
 */
export interface ExtendedMapper<
  DomainEntity extends Entity<any>,
  DBReadModel,
  DBWriteModel,
  DBWriteExtraModel,
> extends PersistenceMapper<DomainEntity, DBReadModel, DBWriteModel> {
  toPersistenceExtra(entity: DomainEntity): DBWriteExtraModel;
}
