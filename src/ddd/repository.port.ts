/*  Most of repositories will probably need generic
    save/find/delete operations, so it's easier
    to have some shared interfaces.
    More specific queries should be defined
    in a respective repository.
*/

export class Paginated<T> {
  readonly total: number;
  readonly perPage: number;
  readonly page: number;
  readonly data: readonly T[];

  constructor(props: Paginated<T>) {
    this.total = props.total;
    this.perPage = props.perPage;
    this.page = props.page;
    this.data = props.data;
  }
}

export type OrderBy = { field: string; param: 'asc' | 'desc' };

export type PaginatedQueryParams = {
  perPage: number;
  page: number;
  orderBy: OrderBy[];
};

export interface RepositoryPort<Entity> {
  findOneById(id: string, include?: any, identifier?: string): Promise<Entity>;
  findOne(where: any, include?: any): Promise<Entity>;
  findAll(where: any, include?: any): Promise<Entity[]>;
  findAllByIds(
    ids: string[],
    include?: any,
    identifier?: string,
  ): Promise<Entity[]>;
  insert(entity: Entity): Promise<void>;
  update(id: string, entity: Entity, identifier?: string): Promise<void>;
  updateWhere(where: any, entity: Entity): Promise<void>;
  delete(entity: Entity, identifier?: string): Promise<boolean>;
  count(where: any): Promise<number>;
  healthCheck(): Promise<boolean>;
  executeQuery?(query: string): Promise<any>;
}

/**
 * Repository Port to be used when model contains unsupported DB types (eg. geography, geometry...)
 */
export interface ExtendedRepositoryPort<Entity> extends RepositoryPort<Entity> {
  insertExtra(
    entity: Entity,
    dbModelName: string,
    identifier?: string,
  ): Promise<void>;
}
