import { OrderBy, PaginatedQueryParams } from './repository.port';

/**
 * Base class for regular queries
 */
export abstract class QueryBase {}

/**
 * Base class for paginated queries
 */
export abstract class PaginatedQueryBase extends QueryBase {
  perPage: number;
  orderBy?: OrderBy[];
  page: number;

  constructor(props: PaginatedParams<PaginatedQueryBase>) {
    super();
    this.perPage = props.perPage || 10;
    this.page = props.page || 0;
    this.orderBy = props.orderBy;
  }
}

// Paginated query parameters
export type PaginatedParams<T> = Omit<T, 'perPage' | 'orderBy' | 'page'> &
  Partial<PaginatedQueryParams>;
