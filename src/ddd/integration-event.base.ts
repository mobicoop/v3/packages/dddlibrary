import { ArgumentNotProvidedException } from '../exceptions';
import { Guard } from '../guard';

type IntegrationEventMetadata = {
  readonly timestamp: number;
  readonly correlationId: string;
  readonly causationId?: string;
  readonly userId?: string;
};

export type IntegrationEventProps<T> = Omit<T, 'id' | 'metadata'> & {
  id: string;
  metadata?: IntegrationEventMetadata;
};

export abstract class IntegrationEvent {
  public readonly id: string;
  public readonly metadata: IntegrationEventMetadata;

  constructor(props: IntegrationEventProps<unknown>) {
    if (Guard.isEmpty(props)) {
      throw new ArgumentNotProvidedException(
        'IntegrationEvent props should not be empty',
      );
    }
    this.id = props.id;
    this.metadata = {
      correlationId: props?.metadata?.correlationId,
      causationId: props?.metadata?.causationId,
      timestamp: props?.metadata?.timestamp || Date.now(),
      userId: props?.metadata?.userId,
    };
  }
}
