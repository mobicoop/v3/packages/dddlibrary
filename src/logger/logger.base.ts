import { LoggerService } from '@nestjs/common';
import { LoggerPort, MessagePublisherPort } from '../ports';

export interface LoggerBaseParams {
  readonly logger: LoggerService;
  readonly prefix?: string;
  readonly domain: string;
  readonly messagePublisher: MessagePublisherPort;
}

export class LoggerBase implements LoggerPort {
  private readonly _prefix: string = 'logging';
  private readonly _domain: string;
  private readonly _messagePublisher: MessagePublisherPort;
  private readonly _logger: LoggerService;

  constructor(protected loggerParams: LoggerBaseParams) {
    this._logger = loggerParams.logger;
    this._prefix = loggerParams.prefix ?? this._prefix;
    this._domain = loggerParams.domain;
    this._messagePublisher = loggerParams.messagePublisher;
  }

  error = (message: any, stack: string, context: string): void => {
    this._messagePublisher.publish(
      `${this._prefix}.${this._domain}.${context}.${LOG_LEVEL.CRITICAL}`,
      stack,
    );
    this._logger.error(message, stack, context);
  };

  warn = (message: any, stack: string, context: string): void => {
    this._messagePublisher.publish(
      `${this._prefix}.${this._domain}.${context}.${LOG_LEVEL.WARNING}`,
      stack,
    );
    this._logger.warn(message, stack, context);
  };

  log = (message: any, context: string): void => {
    this._messagePublisher.publish(
      `${this._prefix}.${this._domain}.${context}.${LOG_LEVEL.INFO}`,
      message,
    );
    this._logger.log(message);
  };

  debug = (message: any): void => {
    this._logger.debug(message);
  };
}

enum LOG_LEVEL {
  CRITICAL = 'crit',
  WARNING = 'warning',
  INFO = 'info',
}
