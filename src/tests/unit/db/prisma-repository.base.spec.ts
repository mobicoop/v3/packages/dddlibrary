import { ResponseBase } from '../../../api/response.base';
import { PrismaRepositoryBase } from '../../../db/prisma-repository.base';
import {
  AggregateID,
  AggregateRoot,
  Mapper,
  RepositoryPort,
} from '../../../ddd';
import {
  ConflictException,
  DatabaseErrorException,
  NotFoundException,
  UniqueConstraintException,
} from '../../../exceptions';
import { Injectable } from '@nestjs/common';
import { EventEmitter2 } from '@nestjs/event-emitter';
import { Test, TestingModule } from '@nestjs/testing';
import { v4 } from 'uuid';

const RECORDS_COUNT = 10;

interface MyEntityProps {
  name: string;
}

interface CreateMyEntityProps {
  name: string;
}

class MyEntity extends AggregateRoot<MyEntityProps> {
  protected readonly _id: AggregateID;

  static create = (create: CreateMyEntityProps): MyEntity => {
    const id = v4();
    const props: MyEntityProps = { ...create };
    const fake = new MyEntity({ id, props });
    return fake;
  };

  validate(): void {
    // not implemented
  }
}

type MyEntityModel = {
  uuid: string;
  name: string;
  createdAt: Date;
  updatedAt: Date;
};

type MyEntityModelWithDedicatedIdentifier = {
  identifier: string;
  name: string;
  createdAt: Date;
  updatedAt: Date;
};

type MyEntityRepositoryPort = RepositoryPort<MyEntity>;

class MyEntityResponseDto extends ResponseBase {
  name: string;
}

const myEntityRecord: MyEntityModel = {
  uuid: 'd567ea3b-4981-43c9-9449-a409b5fa9fed',
  name: 'someName',
  createdAt: new Date('2023-06-28T16:02:00Z'),
  updatedAt: new Date('2023-06-28T16:02:00Z'),
};

const myEntityRecordWithDedicatedIdentifier: MyEntityModelWithDedicatedIdentifier =
  {
    identifier: 'identifier-1',
    name: 'someNameForDedicatedIdentifier',
    createdAt: new Date('2023-06-28T16:02:00Z'),
    updatedAt: new Date('2023-06-28T16:02:00Z'),
  };

let recordId = 2;
const recordUuid = 'uuid-';
const recordName = 'someName-';

const createRandomRecord = (): MyEntityModel => {
  const myEntityRecord: MyEntityModel = {
    uuid: `${recordUuid}${recordId}`,
    name: `${recordName}${recordId}`,
    createdAt: new Date('2023-06-30T08:00:00Z'),
    updatedAt: new Date('2023-06-30T08:00:00Z'),
  };

  recordId++;

  return myEntityRecord;
};

const myEntityRecords: MyEntityModel[] = [];
Array.from({ length: RECORDS_COUNT }).forEach(() => {
  myEntityRecords.push(createRandomRecord());
});

@Injectable()
class MyEntityMapper
  implements Mapper<MyEntity, MyEntityModel, MyEntityModel, MyEntityResponseDto>
{
  toPersistence = (entity: MyEntity): MyEntityModel => {
    const copy = entity.getProps();
    const record: MyEntityModel = {
      uuid: copy.id,
      name: copy.name,
      createdAt: copy.createdAt,
      updatedAt: copy.updatedAt,
    };
    return record;
  };

  toDomain = (record: MyEntityModel): MyEntity => {
    const entity = new MyEntity({
      id: record.uuid,
      createdAt: new Date(record.createdAt),
      updatedAt: new Date(record.updatedAt),
      props: {
        name: record.name,
      },
    });
    return entity;
  };

  toResponse = (entity: MyEntity): MyEntityResponseDto => {
    const props = entity.getProps();
    const response = new MyEntityResponseDto(entity);
    response.name = props.name;
    return response;
  };
}

@Injectable()
class PrismaService {
  myEntity: any;
  $queryRaw: any;
  $queryRawUnsafe: any;
}

const mockPrismaService = {
  $queryRaw: jest
    .fn()
    .mockImplementationOnce(() => {
      return true;
    })
    .mockImplementation(() => {
      throw new Error('Database unavailable');
    })
    .mockImplementationOnce(() => {
      throw new Error();
    }),
  $queryRawUnsafe: jest
    .fn()
    .mockImplementationOnce(() => {
      return {
        uuid: 'd567ea3b-4981-43c9-9449-a409b5fa9fed',
        name: 'someNameFromRawQuery',
      };
    })
    .mockImplementationOnce(() => {
      return [
        {
          uuid: 'd567ea3b-4981-43c9-9449-a409b5fa9fed',
          name: 'someNameFromRawQuery',
        },
        {
          uuid: '7020dd8f-006b-4a05-bf46-510ccb5c3333',
          name: 'someOtherNameFromRawQuery',
        },
      ];
    })
    .mockImplementationOnce(() => {
      throw new Error();
    }),
  myEntity: {
    findUnique: jest.fn().mockImplementation(async (params?: any) => {
      let record: MyEntityModel | MyEntityModelWithDedicatedIdentifier;

      if (params?.where?.uuid) {
        record = myEntityRecords.find(
          (record) => record.uuid === params?.where?.uuid,
        );
      }

      if (params?.where?.identifier) {
        record = myEntityRecordWithDedicatedIdentifier;
      }

      if (!record && params?.where?.uuid == 'uuid-triggering-error') {
        throw new Error('unknown request');
      }

      return record;
    }),
    findFirst: jest.fn().mockImplementation(async (params?: any) => {
      let record: MyEntityModel;

      if (params?.where?.uuid) {
        record = myEntityRecords.find(
          (record) => record.uuid === params?.where?.uuid,
        );
      }

      if (!record && params?.where?.uuid == 'uuid-triggering-error') {
        throw new Error('unknown request');
      }

      return record;
    }),
    findMany: jest
      .fn()
      .mockImplementationOnce(async () => myEntityRecords)
      .mockImplementationOnce(() => {
        throw new Error('An unknown error');
      })
      .mockImplementationOnce(async () => myEntityRecords)
      .mockImplementationOnce(() => {
        throw new Error('An unknown error');
      }),
    create: jest
      .fn()
      .mockResolvedValueOnce(myEntityRecord)
      .mockImplementationOnce(() => {
        throw new Error('Already exists');
      })
      .mockImplementationOnce(() => {
        throw new Error('Unique constraint failed on the fields');
      })
      .mockImplementationOnce(() => {
        throw new Error('An unknown error');
      }),
    update: jest
      .fn()
      // mocks for myEntityRepository.update
      .mockResolvedValueOnce(myEntityRecord)
      .mockResolvedValueOnce(myEntityRecordWithDedicatedIdentifier)
      .mockImplementationOnce(() => {
        throw new Error('Already exists');
      })
      .mockImplementationOnce(() => {
        throw new Error('Unique constraint failed on the fields');
      })
      .mockImplementationOnce(() => {
        throw new Error('An unknown error');
      })
      // mocks for myEntityRepository.updateWhere
      .mockResolvedValueOnce(myEntityRecord)
      .mockImplementationOnce(() => {
        throw new Error('Already exists');
      })
      .mockImplementationOnce(() => {
        throw new Error('Unique constraint failed on the fields');
      })
      .mockImplementationOnce(() => {
        throw new Error('An unknown error');
      }),
    delete: jest
      .fn()
      .mockImplementationOnce(() => true)
      .mockImplementationOnce(() => true)
      .mockImplementationOnce(() => {
        throw new Error('An unknown error');
      }),
    count: jest
      .fn()
      .mockImplementationOnce(() => RECORDS_COUNT)
      .mockImplementationOnce(() => {
        throw new Error('An unknown error');
      }),
  },
};

const mockLogger = {
  log: jest.fn(),
  error: jest.fn(),
  warn: jest.fn(),
  debug: jest.fn(),
};

@Injectable()
class MyEntityRepository
  extends PrismaRepositoryBase<MyEntity, MyEntityModel, MyEntityModel>
  implements MyEntityRepositoryPort
{
  constructor(
    prisma: PrismaService,
    mapper: MyEntityMapper,
    eventEmitter: EventEmitter2,
  ) {
    super(prisma.myEntity, prisma, mapper, eventEmitter, mockLogger);
  }
}

describe('PrismaRepositoryBase', () => {
  let myEntityRepository: MyEntityRepository;
  let prisma: PrismaService;

  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        EventEmitter2,
        MyEntityRepository,
        MyEntityMapper,
        {
          provide: PrismaService,
          useValue: mockPrismaService,
        },
      ],
    }).compile();

    myEntityRepository = module.get<MyEntityRepository>(MyEntityRepository);
    prisma = module.get<PrismaService>(PrismaService);
  });

  afterEach(async () => {
    jest.clearAllMocks();
  });

  it('should be defined', () => {
    expect(myEntityRepository).toBeDefined();
    expect(prisma).toBeDefined();
  });

  describe('findOneById', () => {
    it('should find a record by its id', async () => {
      const record = await myEntityRepository.findOneById('uuid-3');
      expect(record.getProps().name).toBe('someName-3');
    });

    it('should find a record by its id with a dedicated identifier', async () => {
      const record = await myEntityRepository.findOneById(
        'identifier-1',
        null,
        'identifier',
      );
      expect(record.getProps().name).toBe('someNameForDedicatedIdentifier');
    });

    it('should throw a DatabaseErrorException for client error', async () => {
      jest.spyOn(mockLogger, 'error');
      await expect(
        myEntityRepository.findOneById('uuid-triggering-error'),
      ).rejects.toBeInstanceOf(DatabaseErrorException);
      expect(mockLogger.error).toHaveBeenCalledTimes(1);
    });

    it('should throw a NotFoundException if id is not found', async () => {
      jest.spyOn(mockLogger, 'warn');
      await expect(
        myEntityRepository.findOneById('wrong-id'),
      ).rejects.toBeInstanceOf(NotFoundException);
      expect(mockLogger.warn).toHaveBeenCalledTimes(1);
    });
  });

  describe('findOne', () => {
    it('should find a record by properties', async () => {
      const record = await myEntityRepository.findOne({
        uuid: 'uuid-4',
      });
      expect(record.getProps().name).toBe('someName-4');
    });

    it('should throw a DatabaseErrorException for client error', async () => {
      jest.spyOn(mockLogger, 'error');
      await expect(
        myEntityRepository.findOne({
          uuid: 'uuid-triggering-error',
        }),
      ).rejects.toBeInstanceOf(DatabaseErrorException);
      expect(mockLogger.error).toHaveBeenCalledTimes(1);
    });

    it('should throw a NotFoundException if uuid is not found', async () => {
      jest.spyOn(mockLogger, 'warn');
      await expect(
        myEntityRepository.findOne({
          uuid: 'wrong-id',
        }),
      ).rejects.toBeInstanceOf(NotFoundException);
      expect(mockLogger.warn).toHaveBeenCalledTimes(1);
    });
  });

  describe('findAll', () => {
    it('should find all records', async () => {
      const records = await myEntityRepository.findAll({});
      expect(records.length).toBe(RECORDS_COUNT);
    });

    it('should throw an Error if an error occurs', async () => {
      jest.spyOn(mockLogger, 'error');
      await expect(myEntityRepository.findAll({})).rejects.toBeInstanceOf(
        Error,
      );
      expect(mockLogger.error).toHaveBeenCalledTimes(1);
    });
  });

  describe('findAllByIds', () => {
    it('should find all records by their ids', async () => {
      const records = await myEntityRepository.findAllByIds([
        'uuid-1',
        'uuid-2',
        'uuid-3',
        'uuid-4',
        'uuid-5',
        'uuid-6',
        'uuid-7',
        'uuid-8',
        'uuid-9',
        'uuid-10',
      ]);
      expect(records.length).toBe(RECORDS_COUNT);
    });

    it('should throw an Error if an error occurs', async () => {
      jest.spyOn(mockLogger, 'error');
      await expect(
        myEntityRepository.findAllByIds([
          'uuid-1',
          'uuid-2',
          'uuid-3',
          'uuid-4',
          'uuid-5',
          'uuid-6',
          'uuid-7',
          'uuid-8',
          'uuid-9',
          'uuid-10',
        ]),
      ).rejects.toBeInstanceOf(Error);
      expect(mockLogger.error).toHaveBeenCalledTimes(1);
    });
  });

  describe('insert', () => {
    it('should create a record', async () => {
      jest.spyOn(prisma.myEntity, 'create');
      jest.spyOn(mockLogger, 'log');

      await myEntityRepository.insert(
        MyEntity.create({
          name: 'someName',
        }),
      );
      expect(prisma.myEntity.create).toHaveBeenCalledTimes(1);
      expect(mockLogger.log).toHaveBeenCalledTimes(1);
    });

    it('should throw a ConflictException if record already exists', async () => {
      jest.spyOn(mockLogger, 'error');
      await expect(
        myEntityRepository.insert(
          MyEntity.create({
            name: 'someName',
          }),
        ),
      ).rejects.toBeInstanceOf(ConflictException);
      expect(mockLogger.error).toHaveBeenCalledTimes(1);
    });

    it('should throw a UniqueConstraintException if unique constraint fails', async () => {
      jest.spyOn(mockLogger, 'error');
      await expect(
        myEntityRepository.insert(
          MyEntity.create({
            name: 'someName',
          }),
        ),
      ).rejects.toBeInstanceOf(UniqueConstraintException);
      expect(mockLogger.error).toHaveBeenCalledTimes(1);
    });

    it('should throw an Error if an error occurs', async () => {
      jest.spyOn(mockLogger, 'error');
      await expect(
        myEntityRepository.insert(
          MyEntity.create({
            name: 'someName',
          }),
        ),
      ).rejects.toBeInstanceOf(Error);
      expect(mockLogger.error).toHaveBeenCalledTimes(1);
    });
  });

  describe('update', () => {
    const fakeEntity: MyEntity = MyEntity.create({
      name: 'someName',
    });
    it('should update a record', async () => {
      jest.spyOn(prisma.myEntity, 'update');
      jest.spyOn(mockLogger, 'log');
      await myEntityRepository.update(
        'd567ea3b-4981-43c9-9449-a409b5fa9fed',
        fakeEntity,
      );
      expect(prisma.myEntity.update).toHaveBeenCalledTimes(1);
      expect(mockLogger.log).toHaveBeenCalledTimes(1);
    });

    it('should update a record with a dedicated identifier', async () => {
      jest.spyOn(prisma.myEntity, 'update');
      jest.spyOn(mockLogger, 'log');
      await myEntityRepository.update('identifier-1', fakeEntity, 'identifier');
      expect(prisma.myEntity.update).toHaveBeenCalledTimes(1);
      expect(mockLogger.log).toHaveBeenCalledTimes(1);
    });

    it('should throw a ConflictException if record already exists', async () => {
      jest.spyOn(mockLogger, 'error');
      await expect(
        myEntityRepository.update(
          'd567ea3b-4981-43c9-9449-a409b5fa9fed',
          fakeEntity,
        ),
      ).rejects.toBeInstanceOf(ConflictException);
      expect(mockLogger.error).toHaveBeenCalledTimes(1);
    });

    it('should throw a UniqueConstraintException if unique constraint fails', async () => {
      jest.spyOn(mockLogger, 'error');
      await expect(
        myEntityRepository.update(
          'd567ea3b-4981-43c9-9449-a409b5fa9fed',
          fakeEntity,
        ),
      ).rejects.toBeInstanceOf(UniqueConstraintException);
      expect(mockLogger.error).toHaveBeenCalledTimes(1);
    });

    it('should throw an Error if an error occurs', async () => {
      jest.spyOn(mockLogger, 'error');
      await expect(
        myEntityRepository.update(
          'd567ea3b-4981-43c9-9449-a409b5fa9fed',
          fakeEntity,
        ),
      ).rejects.toBeInstanceOf(Error);
      expect(mockLogger.error).toHaveBeenCalledTimes(1);
    });
  });

  describe('updateWhere', () => {
    const fakeEntity: MyEntity = MyEntity.create({
      name: 'someName',
    });
    it('should update a record', async () => {
      jest.spyOn(prisma.myEntity, 'update');
      jest.spyOn(mockLogger, 'log');
      await myEntityRepository.updateWhere(
        {
          uuid: 'd567ea3b-4981-43c9-9449-a409b5fa9fed',
        },
        fakeEntity,
      );
      expect(prisma.myEntity.update).toHaveBeenCalledTimes(1);
      expect(mockLogger.log).toHaveBeenCalledTimes(1);
    });

    it('should throw a ConflictException if record already exists', async () => {
      jest.spyOn(mockLogger, 'error');
      await expect(
        myEntityRepository.updateWhere(
          {
            uuid: 'd567ea3b-4981-43c9-9449-a409b5fa9fed',
          },
          fakeEntity,
        ),
      ).rejects.toBeInstanceOf(ConflictException);
      expect(mockLogger.error).toHaveBeenCalledTimes(1);
    });

    it('should throw a UniqueConstraintException if unique constraint fails', async () => {
      jest.spyOn(mockLogger, 'error');
      await expect(
        myEntityRepository.updateWhere(
          {
            uuid: 'd567ea3b-4981-43c9-9449-a409b5fa9fed',
          },
          fakeEntity,
        ),
      ).rejects.toBeInstanceOf(UniqueConstraintException);
      expect(mockLogger.error).toHaveBeenCalledTimes(1);
    });

    it('should throw an Error if an error occurs', async () => {
      jest.spyOn(mockLogger, 'error');
      await expect(
        myEntityRepository.updateWhere(
          {
            uuid: 'd567ea3b-4981-43c9-9449-a409b5fa9fed',
          },
          fakeEntity,
        ),
      ).rejects.toBeInstanceOf(Error);
      expect(mockLogger.error).toHaveBeenCalledTimes(1);
    });
  });

  describe('delete', () => {
    it('should delete a record', async () => {
      jest.spyOn(prisma.myEntity, 'delete');
      jest.spyOn(mockLogger, 'log');
      const result: boolean = await myEntityRepository.delete(
        MyEntity.create({
          name: 'someName',
        }),
      );
      expect(result).toBeTruthy();
      expect(prisma.myEntity.delete).toHaveBeenCalledTimes(1);
      expect(mockLogger.log).toHaveBeenCalledTimes(1);
    });

    it('should delete a record with a dedicated identifier', async () => {
      jest.spyOn(prisma.myEntity, 'delete');
      jest.spyOn(mockLogger, 'log');
      const result: boolean = await myEntityRepository.delete(
        MyEntity.create({
          name: 'someName',
        }),
        'identifier',
      );
      expect(result).toBeTruthy();
      expect(prisma.myEntity.delete).toHaveBeenCalledTimes(1);
      expect(mockLogger.log).toHaveBeenCalledTimes(1);
    });

    it('should throw a DatabaseErrorException if an error occurs', async () => {
      jest.spyOn(mockLogger, 'error');
      await expect(
        myEntityRepository.delete(
          MyEntity.create({
            name: 'someName',
          }),
        ),
      ).rejects.toBeInstanceOf(DatabaseErrorException);
      expect(mockLogger.error).toHaveBeenCalledTimes(1);
    });
  });

  describe('healthCheck', () => {
    it('should return a healthy result', async () => {
      const res = await myEntityRepository.healthCheck();
      expect(res).toBeTruthy();
    });

    it('should throw an exception if database is not available', async () => {
      jest.spyOn(mockLogger, 'error');
      await expect(myEntityRepository.healthCheck()).rejects.toBeInstanceOf(
        DatabaseErrorException,
      );
      expect(mockLogger.error).toHaveBeenCalledTimes(1);
    });

    it('should throw a DatabaseErrorException if an error occurs', async () => {
      jest.spyOn(mockLogger, 'error');
      await expect(myEntityRepository.healthCheck()).rejects.toBeInstanceOf(
        DatabaseErrorException,
      );
      expect(mockLogger.error).toHaveBeenCalledTimes(1);
    });
  });

  describe('count', () => {
    it('should count records', async () => {
      const res = await myEntityRepository.count({
        name: 'john',
      });
      expect(res).toBe(RECORDS_COUNT);
    });

    it('should throw a DatabaseErrorException if an error occurs', async () => {
      jest.spyOn(mockLogger, 'error');
      await expect(
        myEntityRepository.count({
          name: 'john',
        }),
      ).rejects.toBeInstanceOf(DatabaseErrorException);
      expect(mockLogger.error).toHaveBeenCalledTimes(1);
    });
  });

  describe('findByquery', () => {
    it('should find one record by a raw query', async () => {
      const record = await myEntityRepository.findByQuery('someQueryString');
      expect(record.name).toBe('someNameFromRawQuery');
    });
    it('should find many records by a raw query', async () => {
      const records: [] =
        await myEntityRepository.findByQuery('someQueryString');
      expect(records.length).toBe(2);
    });
    it('should throw a DatabaseErrorException if an error occurs', async () => {
      jest.spyOn(mockLogger, 'error');
      await expect(
        myEntityRepository.findByQuery('someQueryString'),
      ).rejects.toBeInstanceOf(DatabaseErrorException);
      expect(mockLogger.error).toHaveBeenCalledTimes(1);
    });
  });
});
