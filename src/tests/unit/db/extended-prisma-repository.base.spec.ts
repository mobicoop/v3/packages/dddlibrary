import { Injectable } from '@nestjs/common';
import { EventEmitter2 } from '@nestjs/event-emitter';
import { Test, TestingModule } from '@nestjs/testing';
import { v4 } from 'uuid';
import { ResponseBase } from '../../../api/response.base';
import { ExtendedPrismaRepositoryBase } from '../../../db/prisma-repository.base';
import {
  AggregateID,
  AggregateRoot,
  ExtendedMapper,
  ExtendedRepositoryPort,
} from '../../../ddd';
import {
  ConflictException,
  UniqueConstraintException,
} from '../../../exceptions';

// field type unsupported by the ORM as-is
type UnsupportedType = string;

// all properties that a MyEntity has
interface MyEntityProps {
  name: string;
  unsupportedField: UnsupportedType;
}

// properties that are needed for a MyEntity creation
interface CreateMyEntityProps {
  name: string;
  unsupportedField: UnsupportedType;
}

class MyEntity extends AggregateRoot<MyEntityProps> {
  protected readonly _id: AggregateID;

  static create = (create: CreateMyEntityProps): MyEntity => {
    const id = v4();
    const props: MyEntityProps = { ...create };
    const myEntity = new MyEntity({ id, props });
    return myEntity;
  };

  validate(): void {
    // not implemented
  }
}

// all supported fields by the ORM in the persistence system
type MyEntityBaseModel = {
  uuid: string;
  name: string;
  createdAt: Date;
  updatedAt: Date;
};

// all fields that must be retrieved when reading a record
type MyEntityReadModel = MyEntityBaseModel & {
  unsupportedField: string;
};

// all fields required to write a record in the persistence system, using the ORM
type MyEntityWriteModel = MyEntityBaseModel;

// all extra fields (unsupported by ORM) required to write a record
type MyEntityWriteExtraModel = {
  unsupportedField: string;
};

type ExtendedMyEntityRepositoryPort = ExtendedRepositoryPort<MyEntity>;

class MyEntityResponseDto extends ResponseBase {
  name: string;
}

@Injectable()
class MyEntityMapper
  implements
    ExtendedMapper<
      MyEntity,
      MyEntityReadModel,
      MyEntityWriteModel,
      MyEntityWriteExtraModel
    >
{
  toPersistence = (entity: MyEntity): MyEntityWriteModel => {
    const copy = entity.getProps();
    const record: MyEntityWriteModel = {
      uuid: copy.id,
      name: copy.name,
      createdAt: copy.createdAt,
      updatedAt: copy.updatedAt,
    };
    return record;
  };

  toDomain = (record: MyEntityReadModel): MyEntity => {
    const entity = new MyEntity({
      id: record.uuid,
      createdAt: new Date(record.createdAt),
      updatedAt: new Date(record.updatedAt),
      props: {
        name: record.name,
        unsupportedField: record.unsupportedField,
      },
    });
    return entity;
  };

  toPersistenceExtra = (entity: MyEntity): MyEntityWriteExtraModel => ({
    unsupportedField: entity.getProps().unsupportedField,
  });
}

export declare type Value = unknown;
export declare type RawValue = Value | Sql;

declare class Sql {
  values: unknown[];
  strings: string[];
  constructor(
    rawStrings: ReadonlyArray<string>,
    rawValues: ReadonlyArray<RawValue>,
  );
  get text(): string;
  get sql(): string;
  inspect(): {
    text: string;
    sql: string;
    values: unknown[];
  };
}

@Injectable()
class PrismaService {
  myEntity: any;
  $queryRaw: any;
  $queryRawUnsafe: any;
  $executeRawUnsafe: any;
  $transaction: any;
}

const mockPrismaService = {
  $executeRawUnsafe: jest.fn(),
  $transaction: jest
    .fn()
    .mockImplementationOnce(() => ({}))
    .mockImplementationOnce(() => {
      throw new Error('Already exists');
    })
    .mockImplementationOnce(() => {
      throw new Error('Unique constraint failed on the fields');
    })
    .mockImplementationOnce(() => {
      throw new Error('An unknown error');
    }),
  myEntity: {
    create: jest.fn(),
    update: jest.fn(),
  },
};

const mockLogger = {
  log: jest.fn(),
  error: jest.fn(),
  warn: jest.fn(),
  debug: jest.fn(),
};

@Injectable()
class MyEntityRepository
  extends ExtendedPrismaRepositoryBase<
    MyEntity,
    MyEntityReadModel,
    MyEntityWriteModel,
    MyEntityWriteExtraModel
  >
  implements ExtendedMyEntityRepositoryPort
{
  constructor(
    prisma: PrismaService,
    mapper: MyEntityMapper,
    eventEmitter: EventEmitter2,
  ) {
    super(prisma.myEntity, prisma, mapper, eventEmitter, mockLogger);
  }
}

describe('ExtendedPrismaRepositoryBase', () => {
  let myEntityRepository: MyEntityRepository;
  let prisma: PrismaService;

  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        EventEmitter2,
        MyEntityRepository,
        MyEntityMapper,
        {
          provide: PrismaService,
          useValue: mockPrismaService,
        },
      ],
    }).compile();

    myEntityRepository = module.get<MyEntityRepository>(MyEntityRepository);
    prisma = module.get<PrismaService>(PrismaService);
  });

  afterEach(async () => {
    jest.clearAllMocks();
  });

  it('should be defined', () => {
    expect(myEntityRepository).toBeDefined();
    expect(prisma).toBeDefined();
  });

  describe('insertExtra', () => {
    it('should create a record', async () => {
      jest.spyOn(prisma.myEntity, 'create');
      jest.spyOn(mockLogger, 'log');

      await myEntityRepository.insertExtra(
        MyEntity.create({
          name: 'someName',
          unsupportedField: 'someUnsupportedField',
        }),
        'myEntityName',
      );
      expect(prisma.$transaction).toHaveBeenCalledTimes(1);
      expect(prisma.myEntity.create).toHaveBeenCalledTimes(1);
      expect(prisma.$executeRawUnsafe).toHaveBeenCalledTimes(1);
      expect(mockLogger.log).toHaveBeenCalledTimes(1);
    });

    it('should throw a ConflictException if record already exists', async () => {
      jest.spyOn(mockLogger, 'error');
      await expect(
        myEntityRepository.insertExtra(
          MyEntity.create({
            name: 'someName',
            unsupportedField: 'someUnsupportedField',
          }),
          'myEntityName',
        ),
      ).rejects.toBeInstanceOf(ConflictException);
      expect(mockLogger.error).toHaveBeenCalledTimes(1);
    });

    it('should throw a UniqueConstraintException if unique constraint fails', async () => {
      jest.spyOn(mockLogger, 'error');
      await expect(
        myEntityRepository.insertExtra(
          MyEntity.create({
            name: 'someName',
            unsupportedField: 'someUnsupportedField',
          }),
          'myEntityName',
        ),
      ).rejects.toBeInstanceOf(UniqueConstraintException);
      expect(mockLogger.error).toHaveBeenCalledTimes(1);
    });

    it('should throw an Error if an error occurs', async () => {
      jest.spyOn(mockLogger, 'error');
      await expect(
        myEntityRepository.insertExtra(
          MyEntity.create({
            name: 'someName',
            unsupportedField: 'someUnsupportedField',
          }),
          'myEntityName',
        ),
      ).rejects.toBeInstanceOf(Error);
      expect(mockLogger.error).toHaveBeenCalledTimes(1);
    });
  });

  describe('updateExtra', () => {
    it('should update a record', async () => {
      jest.spyOn(prisma.myEntity, 'update');
      jest.spyOn(mockLogger, 'log');

      await myEntityRepository.updateExtra(
        'b2ca28d0-db76-47eb-a23a-f1bd6177e7aa',
        MyEntity.create({
          name: 'someName',
          unsupportedField: 'someUnsupportedField',
        }),
        'myEntityName',
      );
      expect(prisma.$transaction).toHaveBeenCalledTimes(1);
      expect(prisma.myEntity.update).toHaveBeenCalledTimes(1);
      expect(prisma.$executeRawUnsafe).toHaveBeenCalledTimes(1);
      expect(mockLogger.log).toHaveBeenCalledTimes(1);
    });
  });
});
