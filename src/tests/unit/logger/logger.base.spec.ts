import { LoggerBase } from '../../../logger';
import { LoggerBaseParams } from '../../../logger/logger.base';

const mockLogger = {
  log: jest.fn(),
  error: jest.fn(),
  warn: jest.fn(),
  debug: jest.fn(),
};

const mockMessagePublisher = {
  publish: jest.fn().mockImplementation(),
};

const loggerBaseParams: LoggerBaseParams = {
  logger: mockLogger,
  domain: 'fakeDomain',
  messagePublisher: mockMessagePublisher,
};

const loggerBase = new LoggerBase(loggerBaseParams);

describe('Logger Base', () => {
  afterEach(async () => {
    jest.clearAllMocks();
  });

  it('should be defined', () => {
    expect(loggerBase).toBeDefined();
  });

  it('should log an error', () => {
    jest.spyOn(mockLogger, 'error');
    jest.spyOn(mockMessagePublisher, 'publish');
    loggerBase.error(
      'something terrible happened !',
      'some stack informations',
      'myErrorContext',
    );
    expect(mockLogger.error).toHaveBeenCalledTimes(1);
    expect(mockMessagePublisher.publish).toHaveBeenCalledTimes(1);
    expect(mockMessagePublisher.publish).toHaveBeenCalledWith(
      'logging.fakeDomain.myErrorContext.crit',
      'some stack informations',
    );
  });

  it('should log a warning', () => {
    jest.spyOn(mockLogger, 'warn');
    jest.spyOn(mockMessagePublisher, 'publish');
    loggerBase.warn(
      'something not so terrible happened !',
      'some stack informations',
      'myWarningContext',
    );
    expect(mockLogger.warn).toHaveBeenCalledTimes(1);
    expect(mockMessagePublisher.publish).toHaveBeenCalledTimes(1);
    expect(mockMessagePublisher.publish).toHaveBeenCalledWith(
      'logging.fakeDomain.myWarningContext.warning',
      'some stack informations',
    );
  });

  it('should log... a log', () => {
    jest.spyOn(mockLogger, 'log');
    jest.spyOn(mockMessagePublisher, 'publish');
    loggerBase.log('something interesting happened !', 'myContext');
    expect(mockLogger.log).toHaveBeenCalledTimes(1);
    expect(mockMessagePublisher.publish).toHaveBeenCalledTimes(1);
    expect(mockMessagePublisher.publish).toHaveBeenCalledWith(
      'logging.fakeDomain.myContext.info',
      'something interesting happened !',
    );
  });

  it('should log with a new prefix', () => {
    const loggerBaseWithPrefix = new LoggerBase({
      prefix: 'newPrefix',
      ...loggerBaseParams,
    });
    loggerBaseWithPrefix.log('test with prefix', 'myContext');
    jest.spyOn(mockLogger, 'log');
    jest.spyOn(mockMessagePublisher, 'publish');
    expect(mockLogger.log).toHaveBeenCalledTimes(1);
    expect(mockMessagePublisher.publish).toHaveBeenCalledTimes(1);
    expect(mockMessagePublisher.publish).toHaveBeenCalledWith(
      'newPrefix.fakeDomain.myContext.info',
      'test with prefix',
    );
  });

  it('should log debug info', () => {
    jest.spyOn(mockLogger, 'debug');
    jest.spyOn(mockMessagePublisher, 'publish');
    loggerBase.debug('something for debugging');
    expect(mockLogger.debug).toHaveBeenCalledTimes(1);
    expect(mockMessagePublisher.publish).toHaveBeenCalledTimes(0);
  });
});
