import { DomainEvent, DomainEventProps } from '../../../ddd';
import { ArgumentNotProvidedException } from '../../../exceptions';

class FakeDomainEvent extends DomainEvent {
  readonly name: string;

  constructor(props: DomainEventProps<FakeDomainEvent>) {
    super(props);
    this.name = props.name;
  }
}

describe('DomainEvent Base', () => {
  it('should define a domain event based object instance', () => {
    const fakeDomainEvent = new FakeDomainEvent({
      aggregateId: 'some-id',
      name: 'some-name',
    });
    expect(fakeDomainEvent).toBeDefined();
    expect(fakeDomainEvent.id.length).toBe(36);
  });

  it('should define a domain event based object instance with metadata', () => {
    const fakeDomainEvent = new FakeDomainEvent({
      aggregateId: 'some-id',
      name: 'some-name',
      metadata: {
        correlationId: 'some-correlation-id',
        causationId: 'some-causation-id',
        userId: 'some-user-id',
        timestamp: new Date('2023-06-28T05:00:00Z').getTime(),
      },
    });
    expect(fakeDomainEvent.metadata.timestamp).toBe(1687928400000);
  });
  it('should throw an exception if props are empty', () => {
    const emptyProps: DomainEventProps<FakeDomainEvent> = undefined;
    expect(() => new FakeDomainEvent(emptyProps)).toThrow(
      ArgumentNotProvidedException,
    );
  });
});
