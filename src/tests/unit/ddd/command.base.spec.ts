import { Command, CommandProps } from '../../../ddd';
import { ArgumentNotProvidedException } from '../../../exceptions';

class FakeCommand extends Command {
  readonly name: string;

  constructor(props: CommandProps<FakeCommand>) {
    super(props);
    this.name = props.name;
  }
}

class BadFakeCommand extends Command {
  constructor(props: CommandProps<BadFakeCommand>) {
    super(props);
  }
}

describe('Command Base', () => {
  it('should define a command based object instance', () => {
    const fakeCommand = new FakeCommand({ name: 'fakeName' });
    expect(fakeCommand).toBeDefined();
    expect(fakeCommand.id.length).toBe(36);
  });

  it('should define a command based object instance with a provided id', () => {
    const fakeCommand = new FakeCommand({ id: 'some-id', name: 'fakeName' });
    expect(fakeCommand.id).toBe('some-id');
  });

  it('should define a command based object instance with metadata', () => {
    const fakeCommand = new FakeCommand({
      name: 'fakeName',
      metadata: {
        correlationId: 'some-correlation-id',
        causationId: 'some-causation-id',
        userId: 'some-user-id',
        timestamp: new Date('2023-06-28T05:00:00Z').getTime(),
      },
    });
    expect(fakeCommand.metadata.timestamp).toBe(1687928400000);
  });

  it('should throw an exception if props are empty', () => {
    expect(() => new BadFakeCommand({})).toThrow(ArgumentNotProvidedException);
  });
});
