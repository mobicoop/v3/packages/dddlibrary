import {
  PaginatedParams,
  PaginatedQueryBase,
  QueryBase,
} from '../../../ddd/query.base';

class FakeQuery extends QueryBase {
  readonly id: string;

  constructor(id: string) {
    super();
    this.id = id;
  }
}

describe('Query Base', () => {
  it('should define a query based object instance', () => {
    const fakeQuery = new FakeQuery('some-id');
    expect(fakeQuery).toBeDefined();
  });
});

class FakePaginatedQuery extends PaginatedQueryBase {
  readonly id: string;

  constructor(props: PaginatedParams<FakePaginatedQuery>) {
    super(props);
    this.id = props.id;
  }
}

describe('Paginated Query Base', () => {
  it('should define a paginated query based object instance', () => {
    const fakePaginatedQuery = new FakePaginatedQuery({
      id: 'some-id',
      page: 1,
    });
    expect(fakePaginatedQuery).toBeDefined();
  });
});
