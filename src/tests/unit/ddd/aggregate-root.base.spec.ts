import {
  AggregateID,
  AggregateRoot,
  DomainEvent,
  DomainEventProps,
} from '../../../ddd';
import { EventEmitter2 } from '@nestjs/event-emitter';
import { v4 } from 'uuid';

interface FakeProps {
  name: string;
}

interface CreateFakeProps {
  name: string;
}

class FakeRecordCreatedDomainEvent extends DomainEvent {
  readonly name: string;

  constructor(props: DomainEventProps<FakeRecordCreatedDomainEvent>) {
    super(props);
    this.name = props.name;
  }
}

class FakeEntity extends AggregateRoot<FakeProps> {
  protected readonly _id: AggregateID;

  static create = (create: CreateFakeProps): FakeEntity => {
    const id = v4();
    const props: FakeProps = { ...create };
    const fake = new FakeEntity({ id, props });
    fake.addEvent(
      new FakeRecordCreatedDomainEvent({
        aggregateId: id,
        name: props.name,
      }),
    );
    return fake;
  };

  validate(): void {
    // not implemented
  }
}

const mockLogger = {
  debug: jest.fn(),
  log: jest.fn(),
  error: jest.fn(),
  warn: jest.fn(),
};

describe('AggregateRoot Base', () => {
  it('should define an aggregate root based object instance', () => {
    const fakeInstance = FakeEntity.create({
      name: 'someFakeName',
    });
    expect(fakeInstance).toBeDefined();
    expect(fakeInstance.domainEvents.length).toBe(1);
  });

  it('should publish domain events', async () => {
    jest.spyOn(mockLogger, 'debug');
    const eventEmitter = new EventEmitter2();
    jest.spyOn(eventEmitter, 'emitAsync');
    const fakeInstance = FakeEntity.create({
      name: 'someFakeName',
    });
    await fakeInstance.publishEvents(mockLogger, eventEmitter);
    expect(mockLogger.debug).toHaveBeenCalledTimes(1);
    expect(eventEmitter.emitAsync).toHaveBeenCalledTimes(1);
    expect(fakeInstance.domainEvents.length).toBe(0);
  });
});
