import { ValueObject } from '../../../ddd';

interface FakeProps {
  name: string;
  nothing?: string;
}

class FakeValueObject extends ValueObject<FakeProps> {
  get name(): string {
    return this.props.name;
  }

  protected validate(): void {
    return;
  }
}

describe('Value Object Base', () => {
  it('should create a base value object', () => {
    const fakeValueObject = new FakeValueObject({ name: 'fakeName' });
    expect(fakeValueObject).toBeDefined();
    expect(ValueObject.isValueObject(fakeValueObject)).toBeTruthy();
  });

  it('should compare value objects', () => {
    const fakeValueObject = new FakeValueObject({ name: 'fakeName' });
    const fakeValueObjectClone = new FakeValueObject({ name: 'fakeName' });
    const undefinedFakeValueObject: FakeValueObject = undefined;
    const nullFakeValueObject: FakeValueObject = null;
    expect(fakeValueObject.equals(undefinedFakeValueObject)).toBeFalsy();
    expect(fakeValueObject.equals(nullFakeValueObject)).toBeFalsy();
    expect(fakeValueObject.equals(fakeValueObject)).toBeTruthy();
    expect(fakeValueObject.equals(fakeValueObjectClone)).toBeTruthy();
  });

  it('should unpack value object props', () => {
    const fakeValueObject = new FakeValueObject({
      name: 'fakeName',
      nothing: undefined,
    });
    expect(fakeValueObject.unpack()).toEqual({
      name: 'fakeName',
    });
  });
});
