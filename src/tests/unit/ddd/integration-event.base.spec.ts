import {
  IntegrationEvent,
  IntegrationEventProps,
} from '../../../ddd/integration-event.base';
import { ArgumentNotProvidedException } from '../../../exceptions';

class FakeIntegrationEvent extends IntegrationEvent {
  readonly name: string;

  constructor(props: IntegrationEventProps<FakeIntegrationEvent>) {
    super(props);
    this.name = props.name;
  }
}

describe('IntegrationEvent Base', () => {
  it('should define an integration event based object instance', () => {
    const fakeIntegrationEvent = new FakeIntegrationEvent({
      id: '74c0d29f-d444-420c-b34d-a416d2285504',
      name: 'some-name',
    });
    expect(fakeIntegrationEvent).toBeDefined();
    expect(fakeIntegrationEvent.id.length).toBe(36);
  });

  it('should define an integration event based object instance with metadata', () => {
    const fakeIntegrationEvent = new FakeIntegrationEvent({
      id: '74c0d29f-d444-420c-b34d-a416d2285504',
      name: 'some-name',
      metadata: {
        correlationId: 'some-correlation-id',
        causationId: 'some-causation-id',
        userId: 'some-user-id',
        timestamp: new Date('2023-06-28T05:00:00Z').getTime(),
      },
    });
    expect(fakeIntegrationEvent.metadata.timestamp).toBe(1687928400000);
  });
  it('should throw an exception if props are empty', () => {
    const emptyProps: IntegrationEventProps<FakeIntegrationEvent> = undefined;
    expect(() => new FakeIntegrationEvent(emptyProps)).toThrow(
      ArgumentNotProvidedException,
    );
  });
});
