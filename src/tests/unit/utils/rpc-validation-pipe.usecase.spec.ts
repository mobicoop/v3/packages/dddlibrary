import { ArgumentMetadata } from '@nestjs/common';
import { RpcValidationPipe } from '../../../utils/pipes/rpc.validation-pipe';
import { IsNotEmpty, IsString } from 'class-validator';

class SomeDto {
  @IsString()
  @IsNotEmpty()
  id: string;
}

describe('RpcValidationPipe', () => {
  it('should not validate request', async () => {
    const target: RpcValidationPipe = new RpcValidationPipe({
      whitelist: true,
      forbidUnknownValues: false,
    });
    const metadata: ArgumentMetadata = {
      type: 'body',
      metatype: SomeDto,
      data: '',
    };
    await target.transform(<SomeDto>{}, metadata).catch((err) => {
      expect(err.message).toEqual('Rpc Exception');
    });
  });
});
