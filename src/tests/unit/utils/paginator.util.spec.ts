import { Paginator } from '../../../utils/paginator.util';

describe('Paginator Util', () => {
  describe('pageNumber', () => {
    it('should return 0 if number of items is less or equal than the number of items per page', () => {
      const page: number = Paginator.pageNumber(5, 10, 2);
      expect(page).toBe(0);
    });
    it('should return the last page if pageRequired is greater than this number', () => {
      const page: number = Paginator.pageNumber(10, 5, 3);
      expect(page).toBe(1);
    });
    it('should return the pageRequired if it is valid', () => {
      const page: number = Paginator.pageNumber(16, 5, 2);
      expect(page).toBe(2);
    });
  });

  describe('pageItems', () => {
    it('should return a slice of an array', () => {
      const items: number[] = Paginator.pageItems(
        [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
        1,
        5,
      );
      expect(items[0]).toBe(6);
      expect(items[4]).toBe(10);
    });
  });
});
