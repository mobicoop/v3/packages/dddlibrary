import { Guard } from '../../guard';

describe('Guard', () => {
  describe('isEmpty', () => {
    it('should return false for a number', () => {
      expect(Guard.isEmpty(1)).toBeFalsy();
    });
    it('should return false for a falsy boolean', () => {
      expect(Guard.isEmpty(false)).toBeFalsy();
    });
    it('should return false for a truthy boolean', () => {
      expect(Guard.isEmpty(true)).toBeFalsy();
    });
    it('should return true for undefined', () => {
      expect(Guard.isEmpty(undefined)).toBeTruthy();
    });
    it('should return true for null', () => {
      expect(Guard.isEmpty(null)).toBeTruthy();
    });
    it('should return false for a Date', () => {
      expect(Guard.isEmpty(new Date('2023-06-28'))).toBeFalsy();
    });
    it('should return false for an object with keys', () => {
      expect(Guard.isEmpty({ key: 'value' })).toBeFalsy();
    });
    it('should return true for an object without keys', () => {
      expect(Guard.isEmpty({})).toBeTruthy();
    });
    it('should return true for an array without values', () => {
      expect(Guard.isEmpty([])).toBeTruthy();
    });
    it('should return true for an array with only empty values', () => {
      expect(Guard.isEmpty([null, undefined])).toBeTruthy();
    });
    it('should return false for an array with some empty values', () => {
      expect(Guard.isEmpty([1, null, undefined])).toBeFalsy();
    });
    it('should return true for an empty string', () => {
      expect(Guard.isEmpty('')).toBeTruthy();
    });
  });
  describe('lengthIsBetween', () => {
    it('should return true for a string in the range', () => {
      expect(Guard.lengthIsBetween('test', 0, 4)).toBeTruthy();
    });
    it('should return true for a number in the range', () => {
      expect(Guard.lengthIsBetween(2, 0, 4)).toBeTruthy();
    });
    it('should return true for an array with number of elements in the range', () => {
      expect(Guard.lengthIsBetween([1, 2, 3], 0, 4)).toBeTruthy();
    });
    it('should return false for a string not in the range', () => {
      expect(Guard.lengthIsBetween('test', 5, 9)).toBeFalsy();
    });
    it('should return false for a number not in the range', () => {
      expect(Guard.lengthIsBetween(2, 3, 6)).toBeFalsy();
    });
    it('should return false for an array with number of elements not in the range', () => {
      expect(Guard.lengthIsBetween([1, 2, 3], 10, 12)).toBeFalsy();
    });
    it('should throw an exception if value is empty', () => {
      expect(() => Guard.lengthIsBetween(undefined, 0, 4)).toThrow();
    });
  });
});
