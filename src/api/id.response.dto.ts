import { ApiProperty } from '@nestjs/swagger';

export class IdResponse {
  constructor(id: string) {
    this.id = id;
  }

  @ApiProperty({ example: '4702a6b5-3037-4b94-83db-62ccb9945a09' })
  readonly id: string;
}
