import { EventEmitter2 } from '@nestjs/event-emitter';
import {
  AggregateRoot,
  ExtendedMapper,
  ExtendedRepositoryPort,
  PersistenceMapper,
  RepositoryPort,
} from '../ddd';
import {
  ConflictException,
  DatabaseErrorException,
  NotFoundException,
  UniqueConstraintException,
} from '../exceptions';
import { LoggerPort } from '../ports/logger.port';
import {
  PrismaRawRepositoryPort,
  PrismaRepositoryPort,
} from '../ports/prisma-repository.port';
import { ObjectLiteral } from '../types';

export enum LoggerContext {
  READ = 'read',
  CREATED = 'created',
  UPDATED = 'updated',
  DELETED = 'deleted',
  HEALTH = 'health',
}

export abstract class PrismaRepositoryBase<
  Aggregate extends AggregateRoot<any>,
  DBReadModel extends ObjectLiteral,
  DBWriteModel extends ObjectLiteral,
> implements RepositoryPort<Aggregate>
{
  protected _context: string;
  protected readonly SOURCE = 'Prisma';

  protected constructor(
    protected readonly prisma: PrismaRepositoryPort<Aggregate> | any,
    protected readonly prismaRaw: PrismaRawRepositoryPort | any,
    protected readonly mapper: PersistenceMapper<
      Aggregate,
      DBReadModel,
      DBWriteModel
    >,
    protected readonly eventEmitter: EventEmitter2,
    protected readonly logger: LoggerPort,
  ) {}

  findOneById = async (
    id: string,
    include?: any,
    identifier = 'uuid',
  ): Promise<Aggregate> => {
    this._context = LoggerContext.READ;
    try {
      const entity = await this.prisma.findUnique({
        where: { [identifier]: id },
        include,
      });
      if (entity) return this.mapper.toDomain(entity);
      throw new NotFoundException('Record not found');
    } catch (e: any) {
      if (e instanceof NotFoundException) {
        this.logger.warn(
          e.message,
          JSON.stringify({
            source: this.SOURCE,
            id,
            include,
          }),
          this._context,
        );
        throw e;
      }
      this.logger.error(
        e.message,
        JSON.stringify({
          source: this.SOURCE,
          id,
          include,
        }),
        this._context,
      );
      throw new DatabaseErrorException();
    }
  };

  findOne = async (where: any, include?: any): Promise<Aggregate> => {
    this._context = LoggerContext.READ;
    try {
      const entity = await this.prisma.findFirst({
        where,
        include,
      });
      if (entity) return this.mapper.toDomain(entity);
      throw new NotFoundException('Record not found');
    } catch (e: any) {
      if (e instanceof NotFoundException) {
        this.logger.warn(
          e.message,
          JSON.stringify({
            source: this.SOURCE,
            where,
            include,
          }),
          this._context,
        );
        throw e;
      }
      this.logger.error(
        e.message,
        JSON.stringify({
          source: this.SOURCE,
          where,
          include,
        }),
        this._context,
      );
      throw new DatabaseErrorException();
    }
  };

  findAll = async (where: any, include?: any): Promise<Aggregate[]> => {
    this._context = LoggerContext.READ;
    try {
      const entities = await this.prisma.findMany({
        where,
        include,
      });
      return entities.map((entity: DBReadModel) =>
        this.mapper.toDomain(entity),
      );
    } catch (e: any) {
      this.logger.error(
        e.message,
        JSON.stringify({
          source: this.SOURCE,
          where,
          include,
        }),
        this._context,
      );
      throw new DatabaseErrorException();
    }
  };

  findAllByIds = async (
    ids: string[],
    include?: any,
    identifier = 'uuid',
  ): Promise<Aggregate[]> => {
    this._context = LoggerContext.READ;
    try {
      const entities = await this.prisma.findMany({
        where: {
          [identifier]: {
            in: ids,
          },
        },
        include,
      });
      return entities.map((entity: DBReadModel) =>
        this.mapper.toDomain(entity),
      );
    } catch (e: any) {
      this.logger.error(
        e.message,
        JSON.stringify({
          source: this.SOURCE,
          ids,
          include,
        }),
        this._context,
      );
      throw new DatabaseErrorException();
    }
  };

  insert = async (entity: Aggregate): Promise<void> => {
    return this.writeOperation(
      LoggerContext.CREATED,
      entity,
      async (entity: Aggregate) =>
        this.prisma.create({
          data: this.mapper.toPersistence(entity, false),
        }),
    );
  };

  async update(
    id: string,
    entity: Aggregate,
    identifier = 'uuid',
  ): Promise<void> {
    return this.updateWhere({ [identifier]: id }, entity);
  }

  updateWhere = async (where: any, entity: Aggregate): Promise<void> => {
    return this.writeOperation(
      LoggerContext.UPDATED,
      entity,
      async (entity: Aggregate) =>
        this.prisma.update({
          where,
          data: this.mapper.toPersistence(entity, true),
        }),
    );
  };

  delete = async (entity: Aggregate, identifier = 'uuid'): Promise<boolean> => {
    await this.writeOperation(
      LoggerContext.DELETED,
      entity,
      async (entity: Aggregate) =>
        this.prisma.delete({
          where: { [identifier]: entity.id },
        }),
    );
    return true;
  };

  count = async (where: any): Promise<number> => {
    this._context = LoggerContext.READ;
    try {
      const count: number = await this.prisma.count({
        where,
      });
      return count;
    } catch (e: any) {
      this.logger.error(
        e.message,
        JSON.stringify({
          source: this.SOURCE,
          type: 'count',
          where,
        }),
        this._context,
      );
      throw new DatabaseErrorException();
    }
  };

  healthCheck = async (): Promise<boolean> => {
    this._context = LoggerContext.HEALTH;
    try {
      await this.prismaRaw.$queryRaw`SELECT 1`;
      return true;
    } catch (e: any) {
      if (e instanceof Error && e.message) {
        this.logger.error(
          'Health database error',
          JSON.stringify({
            source: this.SOURCE,
            cause: e.message,
            error: e,
          }),
          this._context,
        );
        throw new DatabaseErrorException(e.message);
      }
      this.logger.error(
        'Health database error',
        JSON.stringify({
          source: this.SOURCE,
          error: e,
        }),
        this._context,
      );
      throw new DatabaseErrorException();
    }
  };

  findByQuery = async (query: string): Promise<any> => {
    this._context = LoggerContext.READ;
    try {
      return await this.prismaRaw.$queryRawUnsafe(query);
    } catch (e: any) {
      this.logger.error(
        e.message,
        JSON.stringify({
          source: this.SOURCE,
          rawUnsafeQuery: query,
        }),
        this._context,
      );
      throw new DatabaseErrorException();
    }
  };

  protected async writeOperation(
    context: LoggerContext,
    entity: Aggregate,
    operation: (entity: Aggregate) => Promise<any>,
  ): Promise<void> {
    try {
      await operation(entity);
      entity.publishEvents(this.logger, this.eventEmitter);
      this.logger.log(
        JSON.stringify({
          source: this.SOURCE,
          entity,
        }),
        context,
      );
    } catch (e: any) {
      if (e instanceof Error && e.message.includes('Already exists')) {
        this.logEntityError(context, entity, 'Record already exists', e);
        throw new ConflictException('Record already exists', e);
      }
      if (
        e instanceof Error &&
        e.message.includes('Unique constraint failed on the fields')
      ) {
        this.logEntityError(context, entity, 'Unique constraint failed', e);
        throw new UniqueConstraintException(e.message, e);
      }
      this.logEntityError(context, entity, 'Unknown error', e);
      throw new DatabaseErrorException();
    }
  }

  protected logEntityError(
    context: LoggerContext,
    entity: Aggregate,
    message: string,
    e: Error,
  ) {
    this.logger.error(
      message,
      JSON.stringify({
        source: this.SOURCE,
        cause: e.message,
        error: e,
        entity,
      }),
      context,
    );
  }
}

/**
 * Extra repository methods to handle DB types that are not supported by Prisma
 * TODO why not extending the PrismaRepositoryBase methods instead of adding *Extra methods?
 */
export abstract class ExtendedPrismaRepositoryBase<
    Aggregate extends AggregateRoot<any>,
    DBReadModel extends ObjectLiteral,
    DBWriteModel extends ObjectLiteral,
    DBWriteExtraModel extends ObjectLiteral,
  >
  extends PrismaRepositoryBase<Aggregate, DBReadModel, DBWriteModel>
  implements ExtendedRepositoryPort<Aggregate>
{
  protected constructor(
    protected readonly prisma: PrismaRepositoryPort<Aggregate> | any,
    protected readonly prismaRaw: PrismaRawRepositoryPort,
    protected readonly mapper: ExtendedMapper<
      Aggregate,
      DBReadModel,
      DBWriteModel,
      DBWriteExtraModel
    >,
    protected readonly eventEmitter: EventEmitter2,
    protected readonly logger: LoggerPort,
  ) {
    super(prisma, prismaRaw, mapper, eventEmitter, logger);
  }

  insertExtra = async (
    entity: Aggregate,
    dbModelName: string,
    identifier = 'uuid',
  ): Promise<void> => {
    this._context = LoggerContext.CREATED;
    const baseCommand = this.prisma.create({
      data: this.mapper.toPersistence(entity),
    });
    const updateCommand = this._extraUpdateCommand(
      entity,
      dbModelName,
      identifier,
    );
    return this._extraTransaction(entity, baseCommand, updateCommand);
  };

  updateExtra = async (
    id: string,
    entity: Aggregate,
    dbModelName: string,
    identifier = 'uuid',
  ): Promise<void> => {
    this._context = LoggerContext.UPDATED;
    const baseCommand = this.prisma.update({
      where: {
        [identifier]: id,
      },
      data: this.mapper.toPersistence(entity, true),
    });
    const extraCommand = this._extraUpdateCommand(
      entity,
      dbModelName,
      identifier,
    );
    return this._extraTransaction(entity, baseCommand, extraCommand);
  };

  private _extraUpdateCommand(
    entity: Aggregate,
    dbModelName: string,
    identifier = 'uuid',
  ): string {
    const dbWriteModel = this.mapper.toPersistenceExtra(entity);
    const values = Object.keys(dbWriteModel).map(
      (key) => `${key} = ${dbWriteModel[key]}`,
    );
    const updateCommand = `UPDATE ${dbModelName} SET ${values.join(
      ', ',
    )} WHERE ${identifier} = '${entity.id}'`;
    return updateCommand;
  }

  private _extraTransaction = async (
    entity: Aggregate,
    baseCommand: any,
    extraCommand: string,
  ): Promise<void> => {
    try {
      await this.prismaRaw.$transaction([
        baseCommand,
        this.prismaRaw.$executeRawUnsafe(extraCommand),
      ]);

      entity.publishEvents(this.logger, this.eventEmitter);
      this.logger.log(
        JSON.stringify({
          source: this.SOURCE,
          mappedEntity: this.mapper.toPersistence(entity),
        }),
        this._context,
      );
    } catch (e: any) {
      if (e instanceof Error && e.message.includes('Already exists')) {
        this.logger.error(
          'Record already exists',
          JSON.stringify({
            source: this.SOURCE,
            cause: e.message,
            error: e,
            entity,
          }),
          this._context,
        );
        throw new ConflictException('Record already exists', e);
      }
      if (
        e instanceof Error &&
        e.message.includes('Unique constraint failed on the fields')
      ) {
        this.logger.error(
          'Unique constraint failed',
          JSON.stringify({
            source: this.SOURCE,
            cause: e.message,
            error: e,
            entity,
          }),
          this._context,
        );
        throw new UniqueConstraintException(e.message, e);
      }
      this.logger.error(
        'Unknown error',
        JSON.stringify({
          source: this.SOURCE,
          cause: e.message,
          error: e,
          entity,
        }),
        this._context,
      );
      throw new DatabaseErrorException();
    }
  };
}
